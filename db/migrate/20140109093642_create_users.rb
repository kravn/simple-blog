class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :lastname
      t.string :firstname
      t.string :gender
      t.string :username
      t.string :password
      t.integer :privilege

      t.timestamps
    end
  end
end
