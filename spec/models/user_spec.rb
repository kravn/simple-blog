require 'spec_helper'

describe User do

  context "validations" do
    it { should validate_presence_of(:firstname) }
    it { should validate_presence_of(:lastname) }
    it { should validate_presence_of(:username) }
    it { should validate_presence_of(:password) }
  end

  pending "add some examples to (or delete) #{__FILE__}"

end
