require 'spec_helper'

describe Article do

  context "validations" do
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:article_content) }
  end

  pending "add some examples to (or delete) #{__FILE__}"

end
